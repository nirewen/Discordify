# Discordify

`v3.0.5` by Nirewen

### BetterDiscord Installation:

1. Go to [BetterDiscord's Website](http://betterdiscord.net);
2. Download it for your OS;
3. Install.

### Theme Installation:

1. Download [Discordify.theme.css](https://gitlab.com/nirewen/Discordify/blob/master/Discordify.theme.css) from this repo.
2. Browse to `%appdata%/BetterDiscord/themes` and paste the downloaded file there.
3. Reload Discord (`Ctrl`+ `R`)
4. Open settings and click BetterDiscord tab
5. Click Themes tab on top
6. Enable `Discordify` theme
7. Done!

Note: The theme updates itself, so you only have to reload Discord at every update ;)
#  

#### **Previews:**
![Preview-1](http://nirewen.s-ul.eu/gL0vmNBh.png)

![Preview-2](http://nirewen.s-ul.eu/t47xmAsd.png)

![Preview-3](http://nirewen.s-ul.eu/ZcZR59Vw.png)